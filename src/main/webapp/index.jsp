<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>JenkinsMavenIndex</title>
<script>
var format = 'none';
function showTime() {
	var timeStr = '';
	const now = new Date();
	const year = now.getFullYear();
	const month = now.getMonth() + 1;
	const date = now.getDate();
	const hour = now.getHours();
	const min = now.getMinutes();
	const sec = now.getSeconds();
	timeStr = 'Year: ' + year + ' Month: ' + month + ' Date: ' + date + ' Hour: ' + hour + ' Minute: ' + min + ' Second: ' + sec;
	document.getElementById('dynamicTime').innerText = timeStr;
	format = document.getElementById('formatSelection').value;
	if(format === 'none') {
		document.getElementById('today').innerText = 'Today is: ' + new Date();
	}else if(format === 'dash24') {
		document.getElementById('today').innerText = 'Today is: ' + new Date().yyyyMMddByDash();
	}else if(format === 'slash24') {
		document.getElementById('today').innerText = 'Today is: ' + new Date().yyyyMMddBySlash();
	}else if(format === 'chinese') {
		document.getElementById('today').innerText = 'Today is: ' + new Date().yyyyMMddByChinese();
	}else if(format === 'taiwan') {
		document.getElementById('today').innerText = 'Today is: ' + new Date().yyyyMMddByTaiwan();
	}
	setTimeout('showTime()', 1000);
}
Date.prototype.yyyyMMddByDash = function() {
	const year = this.getFullYear();
	const month = (this.getMonth() + 1) > 9 ? (this.getMonth() + 1) : '0' + (this.getMonth() + 1);
	const date = this.getDate() > 9 ? this.getDate() : '0' + this.getDate();
	const hour = this.getHours() > 9 ? this.getHours() : '0' + this.getHours();
	const min = this.getMinutes() > 9 ? this.getMinutes() : '0' + this.getMinutes();
	const sec = this.getSeconds() > 9 ? this.getSeconds() : '0' + this.getSeconds();
	return year + '-' + month + '-' + date + ' ' + hour + ':' + min + ':' + sec;
}
Date.prototype.yyyyMMddBySlash = function() {
	const year = this.getFullYear();
	const month = (this.getMonth() + 1) > 9 ? (this.getMonth() + 1) : '0' + (this.getMonth() + 1);
	const date = this.getDate() > 9 ? this.getDate() : '0' + this.getDate();
	const hour = this.getHours() > 9 ? this.getHours() : '0' + this.getHours();
	const min = this.getMinutes() > 9 ? this.getMinutes() : '0' + this.getMinutes();
	const sec = this.getSeconds() > 9 ? this.getSeconds() : '0' + this.getSeconds();
	return year + '/' + month + '/' + date + ' ' + hour + ':' + min + ':' + sec;
}
Date.prototype.yyyyMMddByChinese = function() {
	const year = this.getFullYear();
	const month = this.getMonth() + 1;
	const date = this.getDate();
	const hour = this.getHours();
	const min = this.getMinutes();
	const sec = this.getSeconds();
	return year + '年' + month + '月' + date + '日 ' + hour + '時' + min + '分' + sec + '秒';
}
Date.prototype.yyyyMMddByTaiwan = function() {
	const year = this.getFullYear() - 1911;
	const month = this.getMonth() + 1;
	const date = this.getDate();
	const hour = this.getHours();
	const min = this.getMinutes();
	const sec = this.getSeconds();
	return '中華民國' + year + '年' + month + '月' + date + '日 ' + hour + '時' + min + '分' + sec + '秒';
}


function check() {
	var file = document.getElementById('file');
	console.log(file);
}
</script>
</head>
<body style="font-family:courier new" onload="showTime()">
Hi Jenkins, I'm testing tomcat
<input type="file" id="file" />
<button onclick="check()">test</button><hr>
<p id="dynamicTime"></p>
<p id="today"></p><hr>
<p>Select format: 
<select id="formatSelection" onChange="showTime()" style="font-family:courier new">
	<option value="none">none(default)</option>
	<option value="dash24">dash(yyyy-MM-dd HH24:mm:ss)</option>
	<option value="slash24">slash(yyyy/MM/dd HH24:mm:ss)</option>
	<option value="chinese">chinese(yyyy年MM月dd日 HH24時mm分ss秒)</option>
	<option value="taiwan">Taiwan(民國yyy年MM月dd日 HH24時mm分ss秒)</option>
</select>
</p>
</body>
</html>